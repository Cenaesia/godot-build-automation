#!/bin/sh

docker build -f DockerContainerBuilder/Dockerfile --build-arg GODOT_VERSION="${1}" . -t "$2"
docker push $2


