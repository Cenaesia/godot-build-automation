# Advanced Topics

## Using Godot 3.1 (or other version)

There used to be a commitment here to maintain support for older versions of
Godot. I do not believe enough people are using this project for that to be
worth my time. If you need an old version of Godot, let me know and I can likely
add it back in.

## Compiling Godot Engine Itself

This project is not intended to compile Godot Engine itself. There are a few
reasons you might need to do that and you might want to automate that process.
If there is enough interest, next time I need to do that, I'll link the project
here.

## Using A Custom Build of Godot

You might have a custom build of Godot (for example with Modules) that you
would like to use. You could build your own docker container with your version
of Godot installed. I don't do this, instead I use the same docker container
here and overwrite the files I need.

First, I'm assuming you know how to compile Godot for all your target platforms.
I won't cover that here. [Godot has pretty good documentation on the
subject.](https://docs.godotengine.org/en/latest/development/compiling/index.html)

There is one "gotcha" here. You will need to compile "Godot Headless", which is
the Godot version that can run on a server without a screen and build a Godot
project. This is a bit confusing and poorly documented. To get "Godot Headless"
You will need to compile Godot with: `scons platform=server tools=yes` (plus any
other arguements you'd normally use, like `-j` for thread counts and `bits=64`).
If you need "Godot Server", you will need `scons platform=server tools=yes`
instead, and is intended for things like Dedicated Game Servers.

Once you have Godot Headless and the build template for every target platform,
you'll need to modify your `.gitlab-ci.yml` file to be something like this:

```yml
image: registry.gitlab.com/greenfox/godot-build-automation:latest

build:
  script:
  - wget http://hostname.com/customGodotBuild.zip 
    #you can use GitLab to build Godot and use the pages url here^
  - unzip customGodotBuild.zip
  - cp ./.bin/godot_server.x11.opt.tools.64 /usr/local/bin/godot
    # this^ line replaces the godot version in this container with your custom build
  - builder
  artifacts:
    paths:
    - public/*

```

```yml
# where customGodotBuild.zip has:
.bin/godot_server.x11.opt.tools.64
.bin/godot.x11.opt.64
.bin/godot.windows.opt.64.exe
# You'll likely have things like Godot tools in this zip as well, that's fine
```

Then in the Godot editor, you'll need to bring up the Export menu, and point the
Custom Templates for each Export Preset to the appropriate `.bin/godot*` files.
Linux/X11 to `.bin/godot.x11.opt.64`, Windows to
`.bin/godot.windows.opt.64.exe`.

You could also use Git (or Git LFS) to just store these `.bin` files in your Git
Repo. That would be an easy & effective solution. However it is generally
"frowned upon" to store binaries in a Git repo. If you don't often make changes
to your build (like if you're using someone else's modules), it's probably fine
for most projects. You'll still need the `cp
./.bin/godot_server.x11.opt.tools.64 /usr/local/bin/godot` line.
